const express =  require('express');
const app = express();
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');

const server = require('http').Server(app);
const io = require('socket.io')(server);

mongoose.connect('mongodb://localhost:27017/baseapp',{
    useNewUrlParser: true,
});

app.use((req, res, next) => {
    req.io = io;
    next();
})

app.use(cors());

app.use('/file', express.static(path.resolve(__dirname, '..', 'uploads', 'resized')));

app.use(require('./routes'));

server.listen(3333);

